import {Injectable} from "@angular/core";
import {BaseService} from "../../Services/Base.service";
import {User} from "../Models/User.model";
import {Observable} from "rxjs";

@Injectable()
export class UserService extends BaseService<User> {
  protected override baseName = 'users';


  override index(
    pageNumber = 0,
  ): Observable<User[]> {
    const params = this.getGridParams(pageNumber);
    return this.http.get<User[]>(`/api/` + this.baseName, {
      params: params,
    });
  }

  override view(id: number): Observable<User> {
    return this.http.get<User>(`/api/` + this.baseName + `/` + id);
  }
}
