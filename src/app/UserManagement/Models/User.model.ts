export class User {
  public id: number;
  public email: string;
  public first_name: string;
  public last_name: string;
  public avatar: string;

  constructor(Id: number, Email: string, FirstName: string, LastName: string, Avatar: string) {
    this.id = Id;
    this.email = Email;
    this.first_name = FirstName;
    this.last_name = LastName;
    this.avatar = Avatar
  }
}
