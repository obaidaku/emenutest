import {Component, OnInit} from '@angular/core';
import {UserService} from "../../Services/User.service";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../Models/User.model";

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {
  user!: User;

  viewDetails() {
    this.userService.view(this.activeRoute.snapshot.params['id']).subscribe((res: any) => {
      this.user = new User(res.data.id, res.data.email, res.data.first_name, res.data.last_name, res.data.avatar);
    })
  }

  goBack() {
    this.router.navigate(['/Users/index']);
  }

  constructor(private userService: UserService, private activeRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.viewDetails()
  }

}
