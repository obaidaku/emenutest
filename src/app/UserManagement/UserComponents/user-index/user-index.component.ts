import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {User} from "../../Models/User.model";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {UserService} from "../../Services/User.service";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-index',
  templateUrl: './user-index.component.html',
  styleUrls: ['./user-index.component.scss']
})
export class UserIndexComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['id', 'email', 'first name', 'last name', 'avatar', 'view'];
  dataSource!: MatTableDataSource<User>;
  viewType: string = '1';
  cardList!: Observable<any>;
  length: number = 0;
  pageSize: number = 0;
  pageIndex: number = 0;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  users: User[] = [];

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.getUsers()
  }

  getUsers() {
    this.userService.index(this.pageIndex + 1).subscribe((res: any) => {
      this.users = res.data;
      this.dataSource = new MatTableDataSource(this.users);
      this.cardList = this.dataSource.connect();
      this.length = res.total;
      this.pageSize = res.per_page;
    })
  }

  viewUser(id: number) {
    this.router.navigate(['Users/view/' + id]);
  }

  ngAfterViewInit() {
  }

  handlePageEvent(e: PageEvent) {
    this.pageIndex = e.pageIndex;
    this.getUsers();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.cardList = this.dataSource.connect();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
      this.cardList = this.dataSource.connect();
    }
  }
}
