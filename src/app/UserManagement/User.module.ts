import {NgModule} from "@angular/core";
import {UserRouting} from "./User.routing";
import {UserMainComponent} from './UserComponents/user-main/user-main.component';
import {UserService} from "./Services/User.service";
import { UserIndexComponent } from './UserComponents/user-index/user-index.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatRadioModule} from "@angular/material/radio";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {MatCardModule} from "@angular/material/card";
import {MatIconModule} from "@angular/material/icon";
import { UserViewComponent } from './UserComponents/user-view/user-view.component';


@NgModule({
  declarations: [
    UserMainComponent,
    UserIndexComponent,
    UserViewComponent
  ],
  imports: [
    UserRouting,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatRadioModule,
    FormsModule,
    CommonModule,
    MatCardModule,
    MatIconModule
  ],
  providers: [
    UserService
  ],
  exports: [
    UserIndexComponent
  ],
  bootstrap: []
})
export class UserModule {

}
