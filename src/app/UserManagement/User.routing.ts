import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {UserMainComponent} from "./UserComponents/user-main/user-main.component";
import {UserIndexComponent} from "./UserComponents/user-index/user-index.component";
import {UserViewComponent} from "./UserComponents/user-view/user-view.component";

const routes: Routes = [
  {
    path: 'Users', component: UserMainComponent, children: [
      {
        path: 'index', component: UserIndexComponent
      },
      {
        path: 'view/:id', component: UserViewComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UserRouting {

}
