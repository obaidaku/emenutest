import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {ConfigService} from "../Services/config.service";
import {Injectable} from "@angular/core";

@Injectable()

export class Interceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let requestUrl = '';
    requestUrl = req.url.startsWith('/api/') ? this.configService.config.apiUrl + req.url : req.url;
    req = req.clone({
      url: requestUrl
    });
    return next.handle(req)
  }


  constructor(private configService: ConfigService) {
  }
}
