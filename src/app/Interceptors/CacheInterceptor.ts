import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, of, share, tap} from "rxjs";

@Injectable()

export class CacheInterceptor implements HttpInterceptor {
  private cache: Map<string, HttpResponse<any>> = new Map();

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method !== "GET") {
      return next.handle(req)
    }
    const cacheRes: HttpResponse<any> | undefined = this.cache.get(req.urlWithParams);
    if (cacheRes) {
      return of(cacheRes.clone())
    } else {
      return next.handle(req).pipe(
        tap((res) => {
          if (res instanceof HttpResponse) {
            this.cache.set(req.urlWithParams, res)
          }
        }), share());
    }
  }
}
