import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavBarComponent} from "./nav-bar/nav-bar.component";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {UserModule} from "./UserManagement/User.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ConfigService} from "./Services/config.service";
import {Interceptor} from "./Interceptors/Interceptor";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {LoaderComponent} from './loader/loader.component';
import {LoaderInterceptor} from "./Interceptors/LoaderInterceptor";
import {CacheInterceptor} from "./Interceptors/CacheInterceptor";

export function setupConfigServiceFactory(configService: ConfigService): Function {
  return () => configService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    UserModule,
    HttpClientModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: setupConfigServiceFactory,
      deps: [
        ConfigService
      ],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CacheInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
