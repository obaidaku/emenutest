import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

interface Configuration {
  apiUrl?: string;
}

@Injectable({
  providedIn: 'root'
})


export class ConfigService {
  public config: Configuration = {}


  constructor(private http: HttpClient) {
  }


  load(defaults?: Configuration): Promise<Configuration> {
    return new Promise<Configuration>(resolve => {
      this.http.get('config.json').subscribe(
        response => {
          this.config = Object.assign({}, defaults || {}, response || {});
          resolve(this.config);
        },
        () => {
          this.config = Object.assign({}, defaults || {});
          resolve(this.config);
        }
      );
    });
  }
}
