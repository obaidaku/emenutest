import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class BaseService<T> {
  protected baseName!: string;

  constructor(protected http: HttpClient) {
  }

  public getGridParams(pageNumber = 0) {
    let params = new HttpParams()
      .set('page', (pageNumber).toString())
    return params;
  }

  create(obj: any): Observable<T> {
    return this.http.post<T>(`/api/` + this.baseName + `/create`, obj);
  }

  update(id: number, obj: any): Observable<T> {
    return this.http.post<T>(`/api/` + this.baseName + `/update/` + id, obj);
  }

  delete(id: number): Observable<T> {
    return this.http.post<T>(`/api/` + this.baseName + `/delete/` + id, null);
  }

  view(id: number): Observable<T> {
    return this.http.get<T>(`/api/` + this.baseName + `/view/` + id);
  }

  index(
    pageNumber = 0,
  ): Observable<T[]> {
    const params = this.getGridParams(pageNumber);
    return this.http.get<T[]>(`/api/` + this.baseName + `/index`, {
      params: params,
    });
  }
}
